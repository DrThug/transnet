const express = require('express')
const router = express.Router()
const passport = require('passport')
const crypto = require('crypto')
const async = require('async')
const nodemailer = require('nodemailer')
const formidable = require('formidable')
var fs = require('fs');
var ejs = require("ejs");
var smtpTransport = nodemailer.createTransport({
    host: "mail.smartcodegroup.com",
    port: 465,
    secure: 'ssl', // upgrade later with STARTTLS
    auth: {
        user: "no-reply@smartcodegroup.com",
        pass: "63-U5}]K[fB4"
    }
})

//Requiring user model
const Commande = require('../models/commandeModel')
const User = require('../models/userModel')
const Pack = require('../models/packModel')

//Checks if user is authenticated
function isAuthenticatedUser(req, res, next) {
    if (req.isAuthenticated()) {
        return next()
    }
    req.flash('error_msg', 'Veuillez vous connecter pour accéder à cette page.')
    res.redirect('/login')
}

////////////////===== GET ROUTES =====////////////////

router.get('/dashboard/commandes', isAuthenticatedUser, (req, res) => {
    Commande.find({})
        .populate('pack','_id nom montant')
        .populate('user_id','_id name type')
        .sort({ createdAt: -1 })
        .then(lang => {
            res.render('./commandes/index', { page: "Liste des commandes", username: req.user.name, user_admin_id: req.user._id,  menu: "commandes", users: lang })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/api/commandes/:id', (req, res) => {
    Commande.find({user_id:req.params.id})
        .populate('pack','_id nom montant description aide_logement aide_postvisa')
        .populate('user_id','_id name type nom_ecole ville_ecole pays_ecole logo_ecole phone')
        .sort({ createdAt: -1 })
        .then(lang => {
            // console.log(lang);:
            return res.status(200).json({ type: "success", message: 'Commande récupérée avec succès !', data:lang })
            // res.render('./commandes/index', { page: "Liste des commandes", username: req.user.name, user_admin_id: req.user._id,  menu: "commandes", users: lang })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/api/commandes-details/:id', (req, res) => {
    Commande.findOne({_id:req.params.id})
        .populate('pack','_id nom montant description aide_logement aide_postvisa')
        .populate('user_id','_id name type nom_ecole ville_ecole pays_ecole logo_ecole phone')
        .sort({ createdAt: -1 })
        .then(lang => {
            // console.log(lang);:
            return res.status(200).json({ type: "success", message: 'Commande récupérée avec succès !', data:lang })
            // res.render('./commandes/index', { page: "Liste des commandes", username: req.user.name, user_admin_id: req.user._id,  menu: "commandes", users: lang })
        })
        .catch(err => {
            console.log(err)
        })
})

router.get('/api/commandes_ecole/:user', (req, res) => {
    Commande.find({user_id:req.params.user})
        .populate('pack','_id nom montant aide_logement aide_postvisa')
        .populate('user_id','_id name type')
        .sort({ createdAt: -1 })
        .then(lang => {
            // console.log(lang);:
            return res.status(200).json({ type: "success", message: 'Commande récupérée avec succès !', data:lang })
            // res.render('./commandes/index', { page: "Liste des commandes", username: req.user.name, user_admin_id: req.user._id,  menu: "commandes", users: lang })
        })
        .catch(err => {
            console.log(err)
        })
})

////////////////===== POST ROUTES =====////////////////

router.post('/commandes-universcity', (req, res, next) => {
    const form = formidable({ multiples: true });
    var champs = [];
    var filese = [];
    form.on('field', function (fieldName, fieldValue) {
        champs.push({ key: fieldName, value: fieldValue });
    });
    form.parse(req, (err, fields, files) => {
        var uploadedFile = [];
        var category_data = {};
        var i = 0;
        function filtrage(params, type) {
            var filteredArray = [];
            if (type == "field") {
                filteredArray = champs.filter(champ => champ.key == params);
            } else {
                filteredArray = uploadedFile.filter(champ => champ.key == params);
            }
            if (filteredArray.length > 1) {
                var charge = [];
                filteredArray.forEach(element => {
                    charge.push(element.value);
                })
                category_data[params.split('[')[0]] = JSON.stringify(charge);
            } else {
                var charge = filteredArray[0].value;
                category_data[params.split('[')[0]] = params.split('[').length >= 2 ? JSON.stringify([filteredArray[0].value]) : charge;
            }
        }
        champs.forEach(element => {
            // if (element.key != "user_id") {
                filtrage(element.key, "field");
            // }
        });
        function makeid(length) {
            var result           = [];
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
              result.push(characters.charAt(Math.floor(Math.random() * 
         charactersLength)));
           }
           return result.join('');
        }
        category_data["cmd"] = makeid(8);
        // console.log(category_data);
        var tir = __dirname + "/receive_payment_email.ejs";
        tir = tir.replace("routes","views/partials/emails")
        Commande.find({ user_id: category_data.user_id, statut: "Payé" })
            .then(command => {
                console.log(command);
                if (command.length == 0) {
                    if (fields.verify == "oui") {
                        return res.status(200).json({ type: "success", message: 'Commande payable avec succès !' })
                    }else{
                        User.findOne({ _id: category_data.user_id })
                            .then(user => {
                                var commandeur = user;
                                ejs.renderFile(tir, { data: category_data, user: user }, function (err, data) {
                                    let mailOptions = {
                                        to: user.email,
                                        from: 'UniversCity <no-reply@smartcodegroup.com>',
                                        subject: 'Merci pour votre achat !',
                                        html: data
                                    }
                                    smtpTransport.sendMail(mailOptions, err => {
                                        var cmd = new Commande(category_data)
                                        cmd.save().then(resulta => {
                                            Pack.findOne({ _id: category_data.pack })
                                                .then(pack => {
                                                    // console.log(parseInt(pack.option.split('|')[0]));
                                                    var value_option = 0;
                                                    var value_option_renew = 0;
                                                    var value_option_duree = 0;
                                                    var value_option_en_vedette = 0;
                                                    if (commandeur.type == "Ecole") {
                                                        value_option = pack.option_ecole.split('|')[0]
                                                        value_option_renew = pack.option_ecole.split('|')[1]
                                                        value_option_duree = pack.option_ecole.split('|')[2]
                                                        value_option_en_vedette = pack.option_ecole.split('|')[3]
                                                    } else {
                                                        value_option = pack.option_etudiant.split('|')[0]
                                                    }
                                                    User.updateOne({ _id: user._id }, {
                                                        $set: {
                                                            nbre_candidature: parseInt(value_option),
                                                            duree_formation: parseInt(value_option_duree),
                                                            nbre_renew: parseInt(value_option_renew),
                                                            en_vedette: parseInt(value_option_en_vedette),
                                                            essai: pack.essai
                                                        }
                                                    }).then(result => {
                                                        let searchQuery = { _id: user._id }
                                                        User.findOne(searchQuery)
                                                            .then(data_user => {
                                                                return res.status(200).json({ type: "success", message: fields.lang == "en" ? 'Order saved successfully' : 'Commande enregistrée avec succès !', data:data_user })
                                                            })
                                                            .catch(err => {
                                                                return res.status(200).json({ type: "error", message: 'ERROR: ' + err })
                                                            })
                                                    })
                                                    .catch(err => {
                                                        return res.status(200).json({ type: "error", message: 'ERROR: ' + err })
                                                    })
                                                })
                                                .catch(err => {
                                                    console.log(err);
                                                })
                                        })
                                    })
                                })
                            })
                            .catch(err => {
                                console.log(err);
                            })
                    }
                }else{
                    return res.status(200).json({ type: "error", message: fields.lang == "en" ? 'You already have a package in use.' : 'Vous avez déjà un pack en cours d\'utilisation.', })
                }
            })
            .catch(err => {
                console.log(err);
            })
    });
})


module.exports = router