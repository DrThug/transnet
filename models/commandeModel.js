const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema.Types

let cmdSchema = new mongoose.Schema({
    cmd: String,
    statut: String,
    formation_restante: String,
    user_id: {
        type:ObjectId,
        ref:"User"
    },
    pack: {
        type:ObjectId,
        ref:"Pack"
    },
    duree: Date
},{timestamps:true})

module.exports = mongoose.model('Commande', cmdSchema)